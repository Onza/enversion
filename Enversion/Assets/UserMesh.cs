﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class UserMesh {

  [DllImport("Native.dll")]
  protected static extern bool CutTriangles(ref int startTriangleIndex, float[] positivePoints, ref int positiveTriangleCount, int positiveTriangleCapacity, float[] negativePoints, int negativeTriangleCount, int negativeTriangleCapacity);

  public UserMesh(Vector2[] vertices, int triangleCount)
  {
    this.TriangleCount = 0;
    this.TriangleVertices = new float[triangleCount * 2 * 3];
    PrepareAdd(triangleCount);
    for(int i = 0; i < triangleCount * 3; i++)
    {
      Vector2 vert = vertices[i];
      TriangleVertices[(i * 2) + 0] = vert.x;
      TriangleVertices[(i * 2) + 1] = vert.y;
    }
    this.TriangleCount = triangleCount;
  }

  /// <summary>
  /// Constructs a new UserMesh, copying data from an existing one.
  /// </summary>
  /// <param name="existing">The UserMesh which will be copied.</param>
  public UserMesh(UserMesh existing)
  {
    InitializeFrom(existing, existing.TriangleCapacity);
  }

  /// <summary>
  /// Constructs a new UserMesh, copying data from an existing one and allocating 
  /// a specified triangle capacity.
  /// </summary>
  /// <param name="existing">The existing UserMesh which will be copied.</param>
  /// <param name="triangleCapacity">The triangle capacity of the new UserMesh. If this value is less than
  /// the TriangleCapacity of the 'existing' UserMesh parameter, it will be assigned to the existing value.</param>
  public UserMesh(UserMesh existing, int triangleCapacity)
  {
    if (triangleCapacity < existing.TriangleCapacity)
      triangleCapacity = existing.TriangleCapacity;
    InitializeFrom(existing, triangleCapacity);
  }

  /// <summary>
  /// Initializes this UserMesh from an existing UserMesh instance.
  /// </summary>
  /// <param name="existing">The existing UserMesh which will be copied.</param>
  /// <param name="triangleCapacity">The triangle capacity to allocate. If this value is less than the
  /// TriangleCapacity of the 'existing' UserMesh parameter, it will be assigned to the existing value.</param>
  protected void InitializeFrom(UserMesh existing, int triangleCapacity)
  {
    //TODO: How do we ensure that this method is always kept up to date?
    //TODO: Unit tests would help!
    this.TriangleVertices = new float[triangleCapacity * 2 * 3];
    Buffer.BlockCopy(existing.TriangleVertices, 0, this.TriangleVertices, 0, existing.TriangleCount * 2 * 3 * sizeof(float));
    this.TriangleCount = existing.TriangleCount;
  }

  /// <summary>
  /// The initial capacity for the number of triangles to be allocated.
  /// </summary>
  protected static readonly int InitialTriangleCapacity = 8;

  /// <summary>
  /// Array containing the points for each vertex on a triangle. The actual
  /// number of triangles in this array is identified by the TriangleCount 
  /// property. 
  /// </summary>
  /// <remarks>
  /// This array stores the X,Y points for each vertex on a triangle. There are
  /// three vertices for each triangle, and vertices are stored in the X,Y format.
  /// All vertices are stored in a clockwise order. This means that points are stored 
  /// as follows: "Ax, Ay, Bx, By, Cx, Cy".
  /// NOTE: Since three vertices are stored for each triangle, there may be duplicate 
  /// vertices where multiple triangles meet. When generating a Mesh, these duplicate 
  /// vertices should become one single shared vertex.
  /// </remarks>
  protected float[] TriangleVertices { get; private set;  }

  /// <summary>
  /// Gets the current number of triangles in this UserMesh.
  /// </summary>
  protected int TriangleCount { get; private set; }

  /// <summary>
  /// Gets the number of triangles which can currently be stored in the TriangleVertices buffer.
  /// </summary>
  protected int TriangleCapacity
  {
    get
    {
      return (TriangleVertices.Length / 2) / 3;
    }
  }

  /// <summary>
  /// Gets the next size of the TriangleVertices buffer, measured in triangle count,
  /// to be allocated when the current capacity is met.
  /// </summary>
  protected int NextTriangleAllocationCapacity
  {
    get
    {
      if (TriangleCapacity == 0)
        return InitialTriangleCapacity;
      else
        return TriangleCapacity * 2;
    }
  }

  /// <summary>
  /// Gets the size of the TriangleVertices buffer, measured in triangle count,
  /// to be allocated after a sufficient number of triangles are removed.
  /// </summary>
  protected int PreviousTriangleAllocationCapacity
  {
    get
    {
      if (TriangleCapacity == 0)
        return InitialTriangleCapacity;
      else
      {
        int ret = TriangleCapacity / 2;
        if (ret < InitialTriangleCapacity)
          ret = InitialTriangleCapacity;

        return ret;
      }
    }
  }

  /// <summary>
  /// Reallocates the TriangleVertices buffer to store a specific capacity. All current 
  /// triangles will be copied to the new buffer.
  /// </summary>
  /// <param name="triangleCapacity">The maximum number of triangles to support. 
  /// This parameter's value must be greater than or equal to the TriangleCount property's value.</param>
  /// <returns>True if the buffer was successfully allocated, otherwise false.</returns>
  /// <exception cref="ArgumentOutOfRangeException">Thrown if the triangleCapacity parameter's value is less than the current TriangleCount.</exception>
  protected bool Reallocate(int triangleCapacity)
  {
    if (triangleCapacity < TriangleCount)
      throw new ArgumentOutOfRangeException("triangleCapacity", "Cannot allocate fewer triangles than the amount that already exist. See 'TriangleCount'.");

    if (triangleCapacity == this.TriangleCapacity)
      return true;//The amount requested has already been allocated

    try
    {
      float[] newBuffer = new float[triangleCapacity * 2 * 3];
      Buffer.BlockCopy(TriangleVertices, 0, newBuffer, 0, TriangleCount * 2 * 3 * sizeof(float));
      TriangleVertices = newBuffer;
      return true;
    }
    catch(OutOfMemoryException)
    {
      //Could not allocate the requested capacity
      return false;
    }
  }

  /// <summary>
  /// Prepares this UserMesh for adding a specific number of triangles.
  /// </summary>
  /// <param name="triangleCount">The number of triangles for which to prepare.</param>
  /// <returns>True if the UserMesh has been successfully prepared to add the number of triangles, otherwise false.</returns>
  /// <remarks>
  /// This method will determine whether the TriangleVertices array can support the specified amount of triangles.
  /// If the buffer is too small, this method will attempt to expand it to 'NextTriangleAllocationCapacity'. If there
  /// isn't enough memory for 'NextTriangleAllocationCapacity', then it will attempt to allocate the minimum amount
  /// required to support the specified number of triangles. If this fails, the preparation fails and false is returned.
  /// </remarks>
  protected bool PrepareAdd(int triangleCount)
  {
    if(triangleCount + this.TriangleCount > TriangleCapacity)
    {
      //There is not enough space, need to allocate more
      if(false&&Reallocate(NextTriangleAllocationCapacity))//TODO: Remove false&& hack, but it is there for a reason: "NextTriangle..." isn't right! Need to be sure NextTriangle... can store the requested amount!
      {
        //Successfully reallocated
        return true;
      }
      else
      {
        //Failed to allocate the "best guess" size, so try to allocate just the amount required
        int amountRemaining = this.TriangleCapacity - this.TriangleCount;
        int additionalSpaceRequired = triangleCount - amountRemaining;
        return Reallocate(this.TriangleCount + additionalSpaceRequired);
      }
    }
    else
    {
      //There is enough space, so we are prepared.
      return true;
    }
  }

  /// <summary>
  /// Attempts to reduce the size of the TriangleVertices buffer.
  /// </summary>
  protected void Compact()
  {
    if(TriangleCount <= PreviousTriangleAllocationCapacity && PreviousTriangleAllocationCapacity != InitialTriangleCapacity)
    {
      //Currently using considerably more memory than necessary, so reduce it
      Reallocate(PreviousTriangleAllocationCapacity);
    }
  }

  /// <summary>
  /// Gets the vertices of a specific Triangle.
  /// </summary>
  /// <param name="triangleIndex">The index of the triangle.</param>
  /// <param name="a">The first point on the triangle.</param>
  /// <param name="b">The second point on the triangle.</param>
  /// <param name="c">The third point on the triangle.</param>
  /// <returns>True if the triangle index was valid, otherwise false.</returns>
  protected bool GetTriangleVertices(int triangleIndex, out Vector2 a, out Vector2 b, out Vector2 c)
  {
    if(triangleIndex >= 0 && triangleIndex < TriangleCount)
    {
      float ax = TriangleVertices[(triangleIndex * (3 * 2)) + 0];
      float ay = TriangleVertices[(triangleIndex * (3 * 2)) + 1];

      float bx = TriangleVertices[(triangleIndex * (3 * 2)) + 2];
      float by = TriangleVertices[(triangleIndex * (3 * 2)) + 3];

      float cx = TriangleVertices[(triangleIndex * (3 * 2)) + 4];
      float cy = TriangleVertices[(triangleIndex * (3 * 2)) + 5];

      a = new Vector2(ax, ay);
      b = new Vector2(bx, by);
      c = new Vector2(cx, cy);

      return true;
    }
    else
    {
      a = b = c = Vector2.zero;
      //Triangle index out of bounds
      return false;
    }
  }

  protected int EstimateAdditionalTriangles(UserMesh negative)
  {
    return negative.TriangleCount * 9;//Three edges per triangle, three additional triangles per edge
  }

  /// <summary>
  /// Removes mesh based on a specified negative UserMesh.
  /// </summary>
  /// <param name="negative">The negative UserMesh, which will be removed from this UserMesh.</param>
  /// <param name="offsetX">The X offset of the negative UserMesh.</param>
  /// <param name="offsetY">The Y offset of the negative UserMesh.</param>
  /// <returns>True if the UserMesh was successfully cut. False indicates failure such as not enough memory.</returns>
  public bool Cut(UserMesh negative, float offsetX, float offsetY)
  {
    int progress = 0;//Will increment by 'CutTriangles', useful for resuming after failure/reallocation

    int triangleCountRef = this.TriangleCount;
    int loopCount = 0;
    while (loopCount++<100)//TODO: Should not need a loop counter, but what if? Really just trust it to not loop infinitely? So far, seems right. But still scary hypothetical!
    {
      if (!PrepareAdd(EstimateAdditionalTriangles(negative)))//Prepare to add some new triangles (as cutting actually produces more)
      {
        //Failed to allocate the estimated amount required
        //So try to allocate the mimimum
        if(!PrepareAdd(1))
          return false;//Unable to allocate even one triangle, so cannot cut
      }
        
      //'complete' will be false if there was not enough memory. If false, cutting may resume on next iteration after more memory is allocated.
      //TODO: Use native: bool complete = CutTriangles(ref progress, this.TriangleVertices, ref triangleCountRef, this.TriangleCapacity, negative.TriangleVertices, negative.TriangleCount, negative.TriangleCapacity);
      bool complete = NativeEmulator.CutTriangles(ref progress, this.TriangleVertices, ref triangleCountRef, this.TriangleCapacity, negative.TriangleVertices, negative.TriangleCount, negative.TriangleCapacity);
      this.TriangleCount = triangleCountRef;//Update the triangle count
      
      if (complete)
        return true;//All triangles have been cut
    }
    return false;//TODO: Shouldn't need this
  }

  /// <summary>
  /// Adds mesh based on a specified positive UserMesh.
  /// </summary>
  /// <param name="positive">The UserMesh to be added to this UserMesh.</param>
  /// <param name="offsetX">The X offset of the positive UserMesh.</param>
  /// <param name="offsetY">The Y offset of the positive UserMesh.</param>
  /// <returns>True if the UserMesh was successfully added. False indicates failure such as not enough memory.</returns>
  /// <remarks>
  /// This method will create a UserMesh brush, a copy of the positive UserMesh parameter. The brush will
  /// have 'this' UserMesh removed from it. Thus, the brush will only have the area that needs added. This
  /// prevents adding polygons on top of already-existing polygons, which would consume far too much memory.
  /// </remarks>
  public bool Add(UserMesh positive, float offsetX, float offsetY)
  {
    UserMesh brush = new UserMesh(positive);//Create a copy

    if(brush.Cut(this, offsetX, offsetY))//Remove sections which already exist, to prevent merging masses
    {
      return AddBrush(brush, offsetX, offsetY);//Add the additional polygons
    }
    else
    {
      //Failed to cut, so cannot safely add
      return false;
    }
  }
  
  /// <summary>
  /// Adds all polygons from a UserMesh to this UserMesh, offset by a specific amount.
  /// </summary>
  /// <param name="additionalMesh">The positive UserMesh containing the polygons to be copied.
  /// This value should be pre-cut to avoid merging new polygons with already-existing polygons,
  /// which is incredibly inefficient.</param>
  /// <param name="offsetX">The X offset for the positive UserMesh.</param>
  /// <param name="offsetY">The Y offset for the positive UserMesh.</param>
  /// <returns>True if all polygons could be added. False indicates that there was not enough memory to add the triangles.</returns>
  protected bool AddBrush(UserMesh additionalMesh, float offsetX, float offsetY)
  {
    if (!PrepareAdd(additionalMesh.TriangleCount))
      return false;

    for(int i = 0; i < additionalMesh.TriangleCount; i++)
    {
      Vector2 a, b, c;
      additionalMesh.GetTriangleVertices(i, out a, out b, out c);

      int newTriangleIndex = TriangleCount++;
      TriangleVertices[(newTriangleIndex * 2 * 3) + 0] = a.x + offsetX;
      TriangleVertices[(newTriangleIndex * 2 * 3) + 1] = a.y + offsetY;
      TriangleVertices[(newTriangleIndex * 2 * 3) + 2] = b.x + offsetX;
      TriangleVertices[(newTriangleIndex * 2 * 3) + 3] = b.y + offsetY;
      TriangleVertices[(newTriangleIndex * 2 * 3) + 4] = c.x + offsetX;
      TriangleVertices[(newTriangleIndex * 2 * 3) + 5] = c.y + offsetY;
    }

    //TODO: Ideally, we would optimize the mesh at some point, to prevent lag. Why? See below:
    /* Repeatedly adding to fill in gaps means that the gaps still exist in polygons, but they are filled to the edges.
       Imagine a square with a hole in it, then the brush fills that hole. The most efficient shape would then be two triangles
       for the square. But instead what will exist is: A square with a hole cut in it, and a circle filling that hole.
       So optimize this, at least after a continuous edit or so!
    */

    return true;
  }

  public void DebugShow(Color color, float z)
  {
    Color[] colors = new Color[] { Color.black, Color.red, Color.green, Color.blue, Color.cyan, Color.magenta, Color.yellow };
    for(int i = 0; i < TriangleCount; i++)
    {
      //color = colors[i % colors.Length];
      Vector2 a = new Vector2(TriangleVertices[(i * 2 * 3) + 0],
                              TriangleVertices[(i * 2 * 3) + 1]);
      Vector2 b = new Vector2(TriangleVertices[(i * 2 * 3) + 2],
                              TriangleVertices[(i * 2 * 3) + 3]);
      Vector2 c = new Vector2(TriangleVertices[(i * 2 * 3) + 4],
                              TriangleVertices[(i * 2 * 3) + 5]);
      Vector3 a3 = new Vector3(a.x, a.y, z+(i*3));
      Vector3 b3 = new Vector3(b.x, b.y, z+(i*3));
      Vector3 c3 = new Vector3(c.x, c.y, z+(i*3));
      Debug.DrawLine(a3, b3, Color.red);
      Debug.DrawLine(a3, c3, Color.green);
      Debug.DrawLine(c3, b3, Color.blue);
    }
  }
}
