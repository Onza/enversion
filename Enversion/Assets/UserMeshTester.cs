﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class UserMeshTester : MonoBehaviour
{

  public Vector2 LocA;
  public Vector2 LocB;

  public static Vector2 TestLocA;//TODO: Remove these!
  public static Vector2 TestLocB;

  public float curX = 0, curY = 0;

    
void OnGUI()
  {
    Debug.Log("GUI");
    if (Event.current != null)
    {
      
      curX = Event.current.mousePosition.x;
      curY = Event.current.mousePosition.y;
    }
  }

  // Use this for initialization
  void Start () {
    float bigBase = 300.0f;
    float bigHeight = 300.0f;
    UserMesh bigTriangle = new UserMesh(new Vector2[] { new Vector2(0, 0), new Vector2(bigBase / 2.0f, -bigHeight), new Vector2(bigBase, 0) }, 1);

    TestLocA = LocA;
    TestLocB = LocB;

    float smallBase = 30.0f;
    float smallHeight = 30.0f;
    float baseBottomPos = curY;
    float baseLeftPos = curX + ((bigBase - smallBase) / 2.0f);
    UserMesh smallTriangle = new UserMesh(new Vector2[] { new Vector2(baseLeftPos, baseBottomPos), new Vector2(baseLeftPos + (smallBase / 2.0f), baseBottomPos - smallHeight), new Vector2(baseLeftPos + smallBase, baseBottomPos),
    new Vector2(baseLeftPos + (smallBase*2), baseBottomPos), new Vector2(baseLeftPos+(smallBase / 2.0f), baseBottomPos - smallHeight), new Vector2(baseLeftPos+smallBase, baseBottomPos)}, 2);
    if (!bigTriangle.Cut(smallTriangle, 0, 0))
      Debug.Log("Failed to cut!");
    else
    {
      Debug.Log("Cut successful!");
      smallTriangle.DebugShow(Color.yellow, -1.0f);
      bigTriangle.DebugShow(Color.red, 0);

    }
  }

  // Update is called once per frame
  void Update () {
    Start();
  }
}
