﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

static class NativeEmulator
{
  public static bool CutTriangles(ref int startTriangleNegativeIndex, float[] positivePoints, ref int positiveTriangleCount, int positiveTriangleCapacity, float[] negativePoints, int negativeTriangleCount, int negativeTriangleCapacity)
  {
    for (; startTriangleNegativeIndex < negativeTriangleCount; startTriangleNegativeIndex++)
    {
      double neg_Ax = negativePoints[(startTriangleNegativeIndex * (3 * 2)) + 0];
      double neg_Ay = negativePoints[(startTriangleNegativeIndex * (3 * 2)) + 1];
      double neg_Bx = negativePoints[(startTriangleNegativeIndex * (3 * 2)) + 2];
      double neg_By = negativePoints[(startTriangleNegativeIndex * (3 * 2)) + 3];
      double neg_Cx = negativePoints[(startTriangleNegativeIndex * (3 * 2)) + 4];
      double neg_Cy = negativePoints[(startTriangleNegativeIndex * (3 * 2)) + 5];
      
      for (int j = 0; j < positiveTriangleCount; j++)
      {
        if (!LineIntersect(positivePoints, ref positiveTriangleCount, positiveTriangleCapacity, j, neg_Ax, neg_Ay, neg_Bx, neg_By))
          return false;
        if (!LineIntersect(positivePoints, ref positiveTriangleCount, positiveTriangleCapacity, j, neg_Bx, neg_By, neg_Cx, neg_Cy))
          return false;
        if (!LineIntersect(positivePoints, ref positiveTriangleCount, positiveTriangleCapacity, j, neg_Ax, neg_Ay, neg_Cx, neg_Cy))
          return false;
      }
    }

    return true;
  }

  private static void SetTriangle(float[] triangles, int triangleIndex, double Ax, double Ay, double Bx, double By, double Cx, double Cy)
  {
    int triangleIndexEvaluated = triangleIndex;

    if(area(Ax, Ay, Bx, By, Cx, Cy) < 3)//TODO: Arbitrary 3 is bad
    {
      //TODO: After cutting, remove all NaNs
      Ax = Ay = Bx = By = Cx = Cy = double.NaN;
    }

    triangles[(triangleIndexEvaluated * (2 * 3)) + 0] = (float)Ax;
    triangles[(triangleIndexEvaluated * (2 * 3)) + 1] = (float)Ay;
    triangles[(triangleIndexEvaluated * (2 * 3)) + 2] = (float)Bx;
    triangles[(triangleIndexEvaluated * (2 * 3)) + 3] = (float)By;
    triangles[(triangleIndexEvaluated * (2 * 3)) + 4] = (float)Cx;
    triangles[(triangleIndexEvaluated * (2 * 3)) + 5] = (float)Cy;
  }

  public static void DrawDot(double x, double y)
  {
    DrawDot(x, y, 0, Color.green);
  }

  public static void DrawDot(double x, double y, double z, Color color)
  {
    Vector3 TL = new Vector3((float)x - 2, (float)y - 2, (float)z);
    Vector3 TR = new Vector3((float)x + 2, (float)y - 2, (float)z);
    Vector3 BL = new Vector3((float)x - 2, (float)y + 2, (float)z);
    Vector3 BR = new Vector3((float)x + 2, (float)y + 2, (float)z);
    Debug.DrawLine(TL, BR, color);
    Debug.DrawLine(TR, BL, color);
  }

  static void DrawLine(double Ax, double Ay, double Bx, double By, Color color, double z = 0.0)
  {
    Debug.DrawLine(new Vector3((float)Ax, (float)Ay, (float)z), new Vector3((float)Bx, (float)By, (float)z), color);
  }

  static void DrawTriangle(double Ax, double Ay, double Bx, double By, double Cx, double Cy, Color color, double z = 0.0)
  {
    DrawLine(Ax, Ay, Bx, By, color, z);
    DrawLine(Ax, Ay, Cx, Cy, color, z);
    DrawLine(Bx, By, Cx, Cy, color, z);
  }

  static double DistanceFromPointToLine(double x, double y, double Ax, double Ay, double Bx, double By)
  {
    double dx = Ay - Ax;
    double dy = By - Bx;
    if ((dx == 0) && (dy == 0)) return (0.0);
    double t = (dx * (x - Ax) + dy * (y - Bx)) / (dx * dx + dy * dy);
    double m0 = x - Ax - t * dx;
    double m1 = y - Bx - t * dy;
    return (Math.Sqrt(m0 * m0 + m1 * m1));
  }

  private static bool PointsSame(double x, double y, double x2, double y2)
  {
    return Math.Abs(x - x2) < 0.1 && Math.Abs(y - y2) < 0.1;
  }
  
  static bool Parallel(double Ax, double Ay, double Bx, double By, double Cx, double Cy, double Dx, double Dy)
  {
    double abSlope = (By - Ay) * (Dx - Cx);
    double cdSlope = (Dy - Cy) * (Bx - Ax);
    return Math.Abs(abSlope - cdSlope) < 0.0000001;
  }

  static bool Colinear(double x, double y, double Ax, double Ay, double Bx, double By)
  {
    return Math.Abs(area(x, y, Ax, Ay, Bx, By)) < 1.5;//TODO: Use epsilon?
  }

  static bool IsLineSame(double Ax, double Ay, double Bx, double By, double Cx, double Cy, double Dx, double Dy)
  {
    //return Parallel(Ax, Ay, Bx, By, Cx, Cy, Dx, Dy);
    return Colinear(Ax, Ay, Cx, Cy, Dx, Dy) && Colinear(Bx, By, Cx, Cy, Dx, Dy);
  }

  /// <summary>
  /// Pushes a point 'A' infintesimally closer to a point 'B'
  /// </summary>
  static void PushPointTo(ref double Ax, ref double Ay, double Bx, double By)
  {
    bool xPositive = Ax > Bx;
    bool yPositive = Ay > By;
    double distance = 0.0002;//TODO: very bad approximation
    double vec_x = Bx - Ax;
    double vec_y = By - Ay;
    double length = Math.Sqrt(vec_x * vec_x + vec_y * vec_y);
    double unit_vec_x = vec_x / length;
    double unit_vec_y = vec_y / length;

    Ax = Ax + unit_vec_x * distance;
    Ay = Ay + unit_vec_y * distance;
    
    if(xPositive)
    {
      if (Ax < Bx)
        Ax = Bx;
    }
    else
    {
      if (Ax > Bx)
        Ax = Bx;
    }

    if(yPositive)
    {
      if (Ay < By)
        Ay = By;
    }
    else
    {
      if (Ay > By)
        Ay = By;
    }
  }

  /// <summary>
  /// Pushes two points, A and B, closer to a point C
  /// </summary>
  static void PushPointsTo(ref double Ax, ref double Ay, ref double Bx, ref double By, double Cx, double Cy)
  {
    PushPointTo(ref Ax, ref Ay, Cx, Cy);
    PushPointTo(ref Bx, ref By, Cx, Cy);
  }

  private static bool CutAB_AC(float[] positivePoints, ref int positiveTriangleCount, int positiveTriangleCapacity, int positiveTriangleIndex, double ABIntersectionX, double ABIntersectionY, double ACIntersectionX, double ACIntersectionY)
  {
    //TODO: Note that these doubles are here for more reason than just neat naming. They copy!
    double pos_Ax = positivePoints[(positiveTriangleIndex * (3 * 2)) + 0];
    double pos_Ay = positivePoints[(positiveTriangleIndex * (3 * 2)) + 1];
    double pos_Bx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 2];
    double pos_By = positivePoints[(positiveTriangleIndex * (3 * 2)) + 3];
    double pos_Cx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 4];
    double pos_Cy = positivePoints[(positiveTriangleIndex * (3 * 2)) + 5];

    if (positiveTriangleCount + 2/*Replacing one triangle, adding two new*/ > positiveTriangleCapacity)
      return false;//Cannot add the three new triangles

    if (PointsSame(ABIntersectionX, ABIntersectionY, pos_Ax, pos_Ay) || PointsSame(ABIntersectionX, ABIntersectionY, pos_Bx, pos_By) || PointsSame(ABIntersectionX, ABIntersectionY, pos_Cx, pos_Cy) ||
      PointsSame(ACIntersectionX, ACIntersectionY, pos_Ax, pos_Ay) || PointsSame(ACIntersectionX, ACIntersectionY, pos_Bx, pos_By) || PointsSame(ACIntersectionX, ACIntersectionY, pos_Cx, pos_Cy))
    {
      //return true;
    }

    //Have fun figuring what all this does (moves vertices infintesimally away from the AB/AC/BC intersecting line!)
    //These configurations have been written carefully. So tread with caution!
    double ABIntersectionX_TowardA = ABIntersectionX, ABIntersectionY_TowardA = ABIntersectionY;
    double ACIntersectionX_TowardA = ACIntersectionX, ACIntersectionY_TowardA = ACIntersectionY;
    PushPointsTo(ref ABIntersectionX_TowardA, ref ABIntersectionY_TowardA, ref ACIntersectionX_TowardA, ref ACIntersectionY_TowardA, pos_Ax, pos_Ay);

    double ABIntersectionX_TowardB = ABIntersectionX, ABIntersectionY_TowardB = ABIntersectionY;
    double ACIntersectionX_TowardB = ACIntersectionX, ACIntersectionY_TowardB = ACIntersectionY;
    PushPointsTo(ref ABIntersectionX_TowardB, ref ABIntersectionY_TowardB, ref ACIntersectionX_TowardB, ref ACIntersectionY_TowardB, pos_Bx, pos_By);

    double ABIntersectionX_TowardC = ABIntersectionX, ABIntersectionY_TowardC = ABIntersectionY;
    double ACIntersectionX_TowardC = ACIntersectionX, ACIntersectionY_TowardC = ACIntersectionY;
    PushPointsTo(ref ABIntersectionX_TowardC, ref ABIntersectionY_TowardC, ref ACIntersectionX_TowardC, ref ACIntersectionY_TowardC, pos_Cx, pos_Cy);

    SetTriangle(positivePoints, positiveTriangleIndex, pos_Ax, pos_Ay, ABIntersectionX_TowardA, ABIntersectionY_TowardA, ACIntersectionX_TowardA, ACIntersectionY_TowardA);
    SetTriangle(positivePoints, positiveTriangleCount++, ABIntersectionX_TowardB, ABIntersectionY_TowardB, pos_Bx, pos_By, pos_Cx, pos_Cy);
    SetTriangle(positivePoints, positiveTriangleCount++, ACIntersectionX_TowardC, ACIntersectionY_TowardC, ABIntersectionX_TowardB, ABIntersectionY_TowardB, pos_Cx, pos_Cy);
    
    return true;
  }

  private static bool CutAB_BC(float[] positivePoints, ref int positiveTriangleCount, int positiveTriangleCapacity, int positiveTriangleIndex, double ABIntersectionX, double ABIntersectionY, double BCIntersectionX, double BCIntersectionY)
  {
    double pos_Ax = positivePoints[(positiveTriangleIndex * (3 * 2)) + 0];
    double pos_Ay = positivePoints[(positiveTriangleIndex * (3 * 2)) + 1];
    double pos_Bx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 2];
    double pos_By = positivePoints[(positiveTriangleIndex * (3 * 2)) + 3];
    double pos_Cx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 4];
    double pos_Cy = positivePoints[(positiveTriangleIndex * (3 * 2)) + 5];

    if (positiveTriangleCount + 2/*Replacing one triangle, adding two new*/ > positiveTriangleCapacity)
      return false;//Cannot add the three new triangles

    if (PointsSame(ABIntersectionX, ABIntersectionY, pos_Ax, pos_Ay) || PointsSame(ABIntersectionX, ABIntersectionY, pos_Bx, pos_By) || PointsSame(ABIntersectionX, ABIntersectionY, pos_Cx, pos_Cy) ||
      PointsSame(BCIntersectionX, BCIntersectionY, pos_Ax, pos_Ay) || PointsSame(BCIntersectionX, BCIntersectionY, pos_Bx, pos_By) || PointsSame(BCIntersectionX, BCIntersectionY, pos_Cx, pos_Cy))
    {
      //return true;
    }

    //Have fun figuring what all this does (moves vertices infintesimally away from the AB/BC/BC intersecting line!)
    //These configurations have been written carefully. So tread with caution!
    double ABIntersectionX_TowardA = ABIntersectionX, ABIntersectionY_TowardA = ABIntersectionY;
    double BCIntersectionX_TowardA = BCIntersectionX, BCIntersectionY_TowardA = BCIntersectionY;
    PushPointsTo(ref ABIntersectionX_TowardA, ref ABIntersectionY_TowardA, ref BCIntersectionX_TowardA, ref BCIntersectionY_TowardA, pos_Ax, pos_Ay);

    double ABIntersectionX_TowardB = ABIntersectionX, ABIntersectionY_TowardB = ABIntersectionY;
    double BCIntersectionX_TowardB = BCIntersectionX, BCIntersectionY_TowardB = BCIntersectionY;
    PushPointsTo(ref ABIntersectionX_TowardB, ref ABIntersectionY_TowardB, ref BCIntersectionX_TowardB, ref BCIntersectionY_TowardB, pos_Bx, pos_By);

    double ABIntersectionX_TowardC = ABIntersectionX, ABIntersectionY_TowardC = ABIntersectionY;
    double BCIntersectionX_TowardC = BCIntersectionX, BCIntersectionY_TowardC = BCIntersectionY;
    PushPointsTo(ref ABIntersectionX_TowardC, ref ABIntersectionY_TowardC, ref BCIntersectionX_TowardC, ref BCIntersectionY_TowardC, pos_Cx, pos_Cy);

    SetTriangle(positivePoints, positiveTriangleIndex, pos_Ax, pos_Ay, ABIntersectionX_TowardA, ABIntersectionY_TowardA, pos_Cx, pos_Cy);
    SetTriangle(positivePoints, positiveTriangleCount++, ABIntersectionX_TowardB, ABIntersectionY_TowardB, pos_Bx, pos_By, BCIntersectionX_TowardB, BCIntersectionY_TowardB);
    SetTriangle(positivePoints, positiveTriangleCount++, pos_Cx, pos_Cy, ABIntersectionX_TowardA, ABIntersectionY_TowardA, BCIntersectionX_TowardC, BCIntersectionY_TowardC);
    return true;
  }

  private static bool CutAC_BC(float[] positivePoints, ref int positiveTriangleCount, int positiveTriangleCapacity, int positiveTriangleIndex, double ACIntersectionX, double ACIntersectionY, double BCIntersectionX, double BCIntersectionY)
  {
    double pos_Ax = positivePoints[(positiveTriangleIndex * (3 * 2)) + 0];
    double pos_Ay = positivePoints[(positiveTriangleIndex * (3 * 2)) + 1];
    double pos_Bx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 2];
    double pos_By = positivePoints[(positiveTriangleIndex * (3 * 2)) + 3];
    double pos_Cx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 4];
    double pos_Cy = positivePoints[(positiveTriangleIndex * (3 * 2)) + 5];

    if (positiveTriangleCount + 2/*Replacing one triangle, adding two new*/ > positiveTriangleCapacity)
      return false;//Cannot add the three new triangles

    if (PointsSame(BCIntersectionX, BCIntersectionY, pos_Ax, pos_Ay) || PointsSame(BCIntersectionX, BCIntersectionY, pos_Bx, pos_By) || PointsSame(BCIntersectionX, BCIntersectionY, pos_Cx, pos_Cy) ||
      PointsSame(ACIntersectionX, ACIntersectionY, pos_Ax, pos_Ay) || PointsSame(ACIntersectionX, ACIntersectionY, pos_Bx, pos_By) || PointsSame(ACIntersectionX, ACIntersectionY, pos_Cx, pos_Cy))
    {
      //return true;
    }

    //Have fun figuring what all this does (moves vertices infintesimally away from the AC/BC/BC intersecting line!)
    //These configurations have been written carefully. So tread with caution!
    double ACIntersectionX_TowardA = ACIntersectionX, ACIntersectionY_TowardA = ACIntersectionY;
    double BCIntersectionX_TowardA = BCIntersectionX, BCIntersectionY_TowardA = BCIntersectionY;
    PushPointsTo(ref ACIntersectionX_TowardA, ref ACIntersectionY_TowardA, ref BCIntersectionX_TowardA, ref BCIntersectionY_TowardA, pos_Ax, pos_Ay);

    double ACIntersectionX_TowardB = ACIntersectionX, ACIntersectionY_TowardB = ACIntersectionY;
    double BCIntersectionX_TowardB = BCIntersectionX, BCIntersectionY_TowardB = BCIntersectionY;
    PushPointsTo(ref ACIntersectionX_TowardB, ref ACIntersectionY_TowardB, ref BCIntersectionX_TowardB, ref BCIntersectionY_TowardB, pos_Bx, pos_By);

    double ACIntersectionX_TowardC = ACIntersectionX, ACIntersectionY_TowardC = ACIntersectionY;
    double BCIntersectionX_TowardC = BCIntersectionX, BCIntersectionY_TowardC = BCIntersectionY;
    PushPointsTo(ref ACIntersectionX_TowardC, ref ACIntersectionY_TowardC, ref BCIntersectionX_TowardC, ref BCIntersectionY_TowardC, pos_Cx, pos_Cy);

    SetTriangle(positivePoints, positiveTriangleIndex, pos_Ax, pos_Ay, pos_Bx, pos_By, ACIntersectionX_TowardA, ACIntersectionY_TowardA);
    SetTriangle(positivePoints, positiveTriangleCount++, ACIntersectionX_TowardA, ACIntersectionY_TowardA, pos_Bx, pos_By, BCIntersectionX_TowardB, BCIntersectionY_TowardB);
    SetTriangle(positivePoints, positiveTriangleCount++, BCIntersectionX_TowardC, BCIntersectionY_TowardC, pos_Cx, pos_Cy, ACIntersectionX_TowardC, ACIntersectionY_TowardC);
    return true;
  }

  public static double area(double x1, double y1, double x2, double y2, double x3, double y3)
  {
    return Math.Abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0);
  }

  public static bool IsPointInTriangle(double x, double y, double x1, double y1, double x2, double y2, double x3, double y3)
  {
    //TODO: NO! 
    double A = area(x1, y1, x2, y2, x3, y3);
    double A1 = area(x, y, x2, y2, x3, y3);
    double A2 = area(x1, y1, x, y, x3, y3);
    double A3 = area(x1, y1, x2, y2, x, y);
    return (A == A1 + A2 + A3);
  }

  public static bool LineIntersect(float[] positivePoints, ref int positiveTriangleCount, int positiveTriangleCapacity, int positiveTriangleIndex, double Ax, double Ay, double Bx, double By)
  {
    double pos_Ax = positivePoints[(positiveTriangleIndex * (3 * 2)) + 0];
    double pos_Ay = positivePoints[(positiveTriangleIndex * (3 * 2)) + 1];
    double pos_Bx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 2];
    double pos_By = positivePoints[(positiveTriangleIndex * (3 * 2)) + 3];
    double pos_Cx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 4];
    double pos_Cy = positivePoints[(positiveTriangleIndex * (3 * 2)) + 5];


    bool definitelyIntersects = IsPointInTriangle(Ax, Ay, pos_Ax, pos_Ay, pos_Bx, pos_By, pos_Cx, pos_Cy) || IsPointInTriangle(Bx, By, pos_Ax, pos_Ay, pos_Bx, pos_By, pos_Cx, pos_Cy);

    if(IsLineSame(Ax, Ay, Bx, By, pos_Ax, pos_Ay, pos_Bx, pos_By) ||
      IsLineSame(Ax, Ay, Bx, By, pos_Ax, pos_Ay, pos_Cx, pos_Cy) ||
      IsLineSame(Ax, Ay, Bx, By, pos_Bx, pos_By, pos_Cx, pos_Cy) )
    {
      //TODO: Not necessary anymore?
      return true;//No cut is still success
    }

    double ABIntersectionX=0, ABIntersectionY=0;
    double ACIntersectionX = 0, ACIntersectionY = 0;
    double BCIntersectionX = 0, BCIntersectionY = 0;
    if (CheckIntersection(Ax, Ay, Bx, By, definitelyIntersects, pos_Ax, pos_Ay, pos_Bx, pos_By, ref ABIntersectionX, ref ABIntersectionY))
    {
      if (CheckIntersection(Ax, Ay, Bx, By, true, pos_Ax, pos_Ay, pos_Cx, pos_Cy, ref ACIntersectionX, ref ACIntersectionY))
      {
        //Line cuts across AB and AC @ACIntersection and @ABIntersection
        return CutAB_AC(positivePoints, ref positiveTriangleCount, positiveTriangleCapacity, positiveTriangleIndex, ABIntersectionX, ABIntersectionY, ACIntersectionX, ACIntersectionY);
      }
      
      if (CheckIntersection(Ax, Ay, Bx, By, true, pos_Bx, pos_By, pos_Cx, pos_Cy, ref BCIntersectionX, ref BCIntersectionY))
      {
        //Line cuts across AB and BC @BCIntersection and @ABIntersection
        return CutAB_BC(positivePoints, ref positiveTriangleCount, positiveTriangleCapacity, positiveTriangleIndex, ABIntersectionX, ABIntersectionY, BCIntersectionX, BCIntersectionY);
      }
    }

    if (CheckIntersection(Ax, Ay, Bx, By, definitelyIntersects, pos_Ax, pos_Ay, pos_Cx, pos_Cy, ref ACIntersectionX, ref ACIntersectionY))
    {
      //Do not need to check AB intersection, that was already checked above.
      //So the only choice left is BC intersection
      if (CheckIntersection(Ax, Ay, Bx, By, true, pos_Bx, pos_By, pos_Cx, pos_Cy, ref BCIntersectionX, ref BCIntersectionY))
      {
        //Line cuts across AC and BC @BCIntersection and @ACIntersection
        return CutAC_BC(positivePoints, ref positiveTriangleCount, positiveTriangleCapacity, positiveTriangleIndex, ACIntersectionX, ACIntersectionY, BCIntersectionX, BCIntersectionY);
      }
    }
    

    return true;//No cuts were made, but return code indicates success, and no cut is still success
  }

  private static bool DoubleNearZero(double value)
  {
    return Math.Abs(value) < 0.001;//TODO: Proper value?
  }

  private static bool DoesRayIntersectSegment(double rayX1, double rayY1, double rayX2, double rayY2, double segmentX1, double segmentY1, double segmentX2, double segmentY2, ref double intersectionX, ref double intersectionY)
  {
    double rayA = rayY2 - rayY1;
    double rayB = rayX1 - rayX2;
    double rayC = rayA * rayX1 + rayB * rayY1;

    double segmentA = segmentY2 - segmentY1;
    double segmentB = segmentX1 - segmentX2;
    double segmentC = segmentA * segmentX1 + segmentB * segmentY1;

    double det = rayA * segmentB - segmentA * rayB;

    double segmentBoundLeft, segmentBoundRight, segmentBoundTop, segmentBoundBottom;
    if (segmentX1 < segmentX2)
    {
      segmentBoundLeft = segmentX1;
      segmentBoundRight = segmentX2;
    }
    else
    {
      segmentBoundRight = segmentX1;
      segmentBoundLeft = segmentX2;
    }

    if (segmentY1 < segmentY2)
    {
      segmentBoundTop = segmentY1;
      segmentBoundBottom = segmentY2;
    }
    else
    {
      segmentBoundBottom = segmentY1;
      segmentBoundTop = segmentY2;
    }
    
    if (det==0.0)
    {
      return false;
    }
    else
    {
      intersectionX = (segmentB * rayC - rayB * segmentC) / det;
      intersectionY = (rayA * segmentC - segmentA * rayC) / det;

      return intersectionX >= segmentBoundLeft && intersectionX <= segmentBoundRight &&
        intersectionY >= segmentBoundTop && intersectionY <= segmentBoundBottom;
    }
  }

  private static bool DoesSegmentIntersectSegment(double segment1X1, double segment1Y1, double segment1X2, double segment1Y2, double segment2X1, double segment2Y1, double segment2X2, double segment2Y2, ref double intersectionX, ref double intersectionY)
  {
    //First cast 'segment1' to a ray, and see whether that ray intersects segment2
    if (DoesRayIntersectSegment(segment1X1, segment1Y1, segment1X2, segment1Y2, segment2X1, segment2Y1, segment2X2, segment2Y2, ref intersectionX, ref intersectionY))
    {
      //Now check that the intersection points also fit within bounds of 'segment1'
      double segmentBoundLeft, segmentBoundRight, segmentBoundTop, segmentBoundBottom;
      if (segment1X1 < segment1X2)
      {
        segmentBoundLeft = segment1X1;
        segmentBoundRight = segment1X2;
      }
      else
      {
        segmentBoundRight = segment1X1;
        segmentBoundLeft = segment1X2;
      }

      if (segment1Y1 < segment1Y2)
      {
        segmentBoundTop = segment1Y1;
        segmentBoundBottom = segment1Y2;
      }
      else
      {
        segmentBoundBottom = segment1Y1;
        segmentBoundTop = segment1Y2;
      }

      return intersectionX >= segmentBoundLeft && intersectionX <= segmentBoundRight &&
        intersectionY >= segmentBoundTop && intersectionY <= segmentBoundBottom;
    }
    else
    {
      //The 'ray' didn't intersect, thus the segments do not intersect either
      return false;
    }
  }

  private static bool CheckIntersection(double segment1X1, double segment1Y1, double segment1X2, double segment1Y2, bool segment1IsRay, double segment2X1, double segment2Y1, double segment2X2, double segment2Y2, ref double intersectionX, ref double intersectionY)
  {
    if (segment1IsRay)
      return DoesRayIntersectSegment(segment1X1, segment1Y1, segment1X2, segment1Y2, segment2X1, segment2Y1, segment2X2, segment2Y2, ref intersectionX, ref intersectionY);
    else
      return DoesSegmentIntersectSegment(segment1X1, segment1Y1, segment1X2, segment1Y2, segment2X1, segment2Y1, segment2X2, segment2Y2, ref intersectionX, ref intersectionY);
  }
}