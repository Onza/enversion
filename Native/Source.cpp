#include "Source.h"

#define min(a,b) ((a<b)?a:b)
#define max(a,b) ((a>b)?a:b)
#define EPSILON (1.95f)//TODO:NO WAY

#define DoubleNearZero(value) (fabsf(value-0.0) < EPSILON)

#define SetTriangle(triangles, triangleIndex, ax, ay, bx, by, cx, cy){\
int triangleIndexEvaluated = triangleIndex; \
triangles[(triangleIndexEvaluated * (2 * 3)) + 0] = ax; \
triangles[(triangleIndexEvaluated * (2 * 3)) + 1] = ay; \
triangles[(triangleIndexEvaluated * (2 * 3)) + 2] = bx; \
triangles[(triangleIndexEvaluated * (2 * 3)) + 3] = by; \
triangles[(triangleIndexEvaluated * (2 * 3)) + 4] = cx; \
triangles[(triangleIndexEvaluated * (2 * 3)) + 5] = cy; }

bool DoesRayIntersectSegment(double rayX1, double rayY1, double rayX2, double rayY2, double segmentX1, double segmentY1, double segmentX2, double segmentY2, double &intersectionX, double &intersectionY)
{
  double rayA = rayY2 - rayY1;
  double rayB = rayX1 - rayX2;
  double rayC = rayA*rayX1 + rayB*rayY1;

  double segmentA = segmentY2 - segmentY1;
  double segmentB = segmentX1 - segmentX2;
  double segmentC = segmentA*segmentX1 + segmentB*segmentY1;

  double det = rayA*segmentB - segmentA*rayB;

  double segmentBoundLeft, segmentBoundRight, segmentBoundTop, segmentBoundBottom;
  if (segmentX1 < segmentX2)
  {
    segmentBoundLeft = segmentX1;
    segmentBoundRight = segmentX2;
  }
  else
  {
    segmentBoundRight = segmentX1;
    segmentBoundLeft = segmentX2;
  }

  if (segmentY1 < segmentY2)
  {
    segmentBoundTop = segmentY1;
    segmentBoundBottom = segmentY2;
  }
  else
  {
    segmentBoundBottom = segmentY1;
    segmentBoundTop = segmentY2;
  }

  if (DoubleNearZero(det))
  {
    //Parallel lines do not intersect!
    return false;
  }
  else
  {
    intersectionX = (segmentB*rayC - rayB*segmentC)/det;
    intersectionY = (rayA*segmentC - segmentA*rayC)/det;

    return intersectionX >= segmentBoundLeft && intersectionX <= segmentBoundRight &&
      intersectionY >= segmentBoundTop && intersectionY <= segmentBoundBottom;
  }
}

double DistanceFromPointToLine(double x, double y, double Ax, double Ay, double Bx, double By)
{
  double dx = Ay - Ax;
  double dy = By - Bx;
  if ((dx == 0) && (dy == 0)) return(0.0);
  double t = (dx*(x - Ax) + dy*(y - Bx)) / (dx*dx + dy*dy);
  double m0 = x - Ax - t*dx;
  double m1 = y - Bx - t*dy;
  return(sqrt(m0*m0 + m1*m1));
}



bool Parallel(double Ax, double Ay, double Bx, double By, double Cx, double Cy, double Dx, double Dy)
{
  if (Dx-Cx==0.0 || Bx-Ax==0.0)
  {
    Beep(1500, 1000);
    Beep(500, 500);
    Beep(800, 500);
  }
  double abSlope = (By - Ay) / (Bx - Ax);
  double cdSlope = (Dy - Cy) / (Dx - Cx);
  return fabsf(abSlope - cdSlope) < 0.01;//TODO: Use better value!
}

bool PointOnLine(double x, double y, double Ax, double Ay, double Bx, double By)
{
  return DistanceFromPointToLine(x, y, Ax, Ay, Bx, By) < 10;//TODO: Not tested. Refused to be realistic
}

bool PointEqual(double Ax, double Ay, double Bx, double By)
{
  return fabsf(Ax - Bx) < EPSILON && fabsf(Ay - By) < EPSILON;
}

bool LinesEqual(double Ax, double Ay, double Bx, double By, double Cx, double Cy, double Dx, double Dy)
{
  if (Parallel(Ax, Ay, Bx, By, Cx, Cy, Dx, Dy))
  {
    double abSlope = (By - Ay) / (Bx - Ax);
    double cdSlope = (Dy - Cy) / (Dx - Cx);
    double ab_b = (-1.0)*abSlope*Bx + By;
    double cd_b = (-1.0)*cdSlope*Dx + Dy;
    
    if (fabsf(ab_b - cd_b) < 1)//TODO: Again, real value
    {
      Beep(500, 200);
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    
    return false;
  }
}

bool DoesSegmentIntersectSegment(double segment1X1, double segment1Y1, double segment1X2, double segment1Y2, double segment2X1, double segment2Y1, double segment2X2, double segment2Y2, double &intersectionX, double& intersectionY)
{
  //First cast 'segment1' to a ray, and see whether that ray intersects segment2
  if (DoesRayIntersectSegment(segment1X1, segment1Y1, segment1X2, segment1Y2, segment2X1, segment2Y1, segment2X2, segment2Y2, intersectionX, intersectionY))
  {
    //Now check that the intersection points also fit within bounds of 'segment1'
    double segmentBoundLeft, segmentBoundRight, segmentBoundTop, segmentBoundBottom;
    if (segment1X1 < segment1X2)
    {
      segmentBoundLeft = segment1X1;
      segmentBoundRight = segment1X2;
    }
    else
    {
      segmentBoundRight = segment1X1;
      segmentBoundLeft = segment1X2;
    }

    if (segment1Y1 < segment1Y2)
    {
      segmentBoundTop = segment1Y1;
      segmentBoundBottom = segment1Y2;
    }
    else
    {
      segmentBoundBottom = segment1Y1;
      segmentBoundTop = segment1Y2;
    }

    return intersectionX >= segmentBoundLeft && intersectionX <= segmentBoundRight &&
      intersectionY >= segmentBoundTop && intersectionY <= segmentBoundBottom;
  }
  else
  {
    //The 'ray' didn't intersect, thus the segments do not intersect either
    return false;
  }
}

bool CheckIntersection(double segment1X1, double segment1Y1, double segment1X2, double segment1Y2, bool segment1IsRay, double segment2X1, double segment2Y1, double segment2X2, double segment2Y2, double &intersectionX, double& intersectionY)
{

  if (segment1IsRay)
    return DoesRayIntersectSegment(segment1X1, segment1Y1, segment1X2, segment1Y2, segment2X1, segment2Y1, segment2X2, segment2Y2, intersectionX, intersectionY);
  else
    return DoesSegmentIntersectSegment(segment1X1, segment1Y1, segment1X2, segment1Y2, segment2X1, segment2Y1, segment2X2, segment2Y2, intersectionX, intersectionY);
}

double area(double x1, double y1, double x2, double y2, double x3, double y3)
{
  return abs((x1*(y2 - y3) + x2*(y3 - y1) + x3*(y1 - y2)) / 2.0);
}

double Orientation(double x1, double y1, double x2, double y2, double x3, double y3)
{
  return (x1 - x3) * (y2 - y3) - (x2 - x3) * (y1 - y3);
}

bool IsPointInTriangle(double x, double y, double x1, double y1, double x2, double y2, double x3, double y3)
{
  //TODO: NO! 
  /* Calculate area of triangle ABC */
  float A = area(x1, y1, x2, y2, x3, y3);

  /* Calculate area of triangle PBC */
  float A1 = area(x, y, x2, y2, x3, y3);

  /* Calculate area of triangle PAC */
  float A2 = area(x1, y1, x, y, x3, y3);

  /* Calculate area of triangle PAB */
  float A3 = area(x1, y1, x2, y2, x, y);

  /* Check if sum of A1, A2 and A3 is same as A */
  return (A == A1 + A2 + A3);
}

extern "C"
{
  bool CutTriangles(int& startTriangleNegativeIndex, float* positivePoints, int &positiveTriangleCount, int positiveTriangleCapacity, float* negativePoints, int negativeTriangleCount, int negativeTriangleCapacity)
  {
    int opt = positiveTriangleCount;//TODO: NO NOT THIS HACK
    for (; startTriangleNegativeIndex < negativeTriangleCount; startTriangleNegativeIndex++)
    {
      double neg_Ax = negativePoints[( startTriangleNegativeIndex * (3 * 2) ) + 0];
      double neg_Ay = negativePoints[( startTriangleNegativeIndex * (3 * 2) ) + 1];
      double neg_Bx = negativePoints[( startTriangleNegativeIndex * (3 * 2) ) + 2];
      double neg_By = negativePoints[( startTriangleNegativeIndex * (3 * 2) ) + 3];
      double neg_Cx = negativePoints[( startTriangleNegativeIndex * (3 * 2) ) + 4];
      double neg_Cy = negativePoints[( startTriangleNegativeIndex * (3 * 2) ) + 5];

      for (int j = 0; j < positiveTriangleCount && j < opt + 480; j++)
      {
        if (!LineIntersect(positivePoints, positiveTriangleCount, positiveTriangleCapacity, j, neg_Ax, neg_Ay, neg_Bx, neg_By))
          return false;
        if (!LineIntersect(positivePoints, positiveTriangleCount, positiveTriangleCapacity, j, neg_Bx, neg_By, neg_Cx, neg_Cy))
          return false;
        if (!LineIntersect(positivePoints, positiveTriangleCount, positiveTriangleCapacity, j, neg_Ax, neg_Ay, neg_Cx, neg_Cy))
          return false;
      }
    }

    return true;
  }
}

bool CutAB_AC(float* positivePoints, int &positiveTriangleCount, int positiveTriangleCapacity, int positiveTriangleIndex, double ABIntersectionX, double ABIntersectionY, double ACIntersectionX, double ACIntersectionY)
{
  double pos_Ax = positivePoints[(positiveTriangleIndex * (3 * 2)) + 0];
  double pos_Ay = positivePoints[(positiveTriangleIndex * (3 * 2)) + 1];
  double pos_Bx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 2];
  double pos_By = positivePoints[(positiveTriangleIndex * (3 * 2)) + 3];
  double pos_Cx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 4];
  double pos_Cy = positivePoints[(positiveTriangleIndex * (3 * 2)) + 5];

  if (positiveTriangleCount + 2/*Replacing one triangle, adding two new*/ > positiveTriangleCapacity)
    return false;//Cannot add the three new triangles

  if (PointEqual(ABIntersectionX, ABIntersectionY, ACIntersectionX, ACIntersectionY))
  {
    return true;
  }

  if (PointEqual(ACIntersectionX, ACIntersectionY, pos_Ax, pos_Ay)
    || PointEqual(ACIntersectionX, ACIntersectionY, pos_Bx, pos_By)
    || PointEqual(ACIntersectionX, ACIntersectionY, pos_Cx, pos_Cy))
  {
    if (IsPointInTriangle(ABIntersectionX, ABIntersectionY, pos_Ax, pos_Ay, pos_Bx, pos_By, pos_Cx, pos_Cy))
      return true;//Mistaken intersection
  }

  if (PointEqual(ABIntersectionX, ABIntersectionY, pos_Ax, pos_Ay)
    || PointEqual(ABIntersectionX, ABIntersectionY, pos_Bx, pos_By)
    || PointEqual(ABIntersectionX, ABIntersectionY, pos_Cx, pos_Cy))
  {
    if (IsPointInTriangle(ACIntersectionX, ACIntersectionY, pos_Ax, pos_Ay, pos_Bx, pos_By, pos_Cx, pos_Cy))
      return true;//Mistaken intersection
  }

  if (LinesEqual(ABIntersectionX, ABIntersectionY, ACIntersectionX, ACIntersectionY, pos_Ax, pos_Ay, pos_Bx, pos_By))
  {
    //Both intersection points are on the AB line of the triangle, so no need to cut
    return true;
  }

  if (LinesEqual(ABIntersectionX, ABIntersectionY, ACIntersectionX, ACIntersectionY, pos_Ax, pos_Ay, pos_Cx, pos_Cy))
  {
    //Both intersection points are on the AB line of the triangle, so no need to cut
    return true;
  }

  if (LinesEqual(ABIntersectionX, ABIntersectionY, ACIntersectionX, ACIntersectionY, pos_Bx, pos_By, pos_Cx, pos_Cy))
  {
    //Both intersection points are on the AB line of the triangle, so no need to cut
    return true;
  }

  //TODO: On first of each of these three-block calls, replace 'positiveTriangleCount++' with positiveTriangleIndex
  SetTriangle(positivePoints, positiveTriangleIndex, pos_Ax, pos_Ay, ABIntersectionX, ABIntersectionY, ACIntersectionX, ACIntersectionY);
  SetTriangle(positivePoints, positiveTriangleCount++, ABIntersectionX, ABIntersectionY, pos_Bx, pos_By, pos_Cx, pos_Cy);
  SetTriangle(positivePoints, positiveTriangleCount++, ACIntersectionX, ACIntersectionY, ABIntersectionX, ABIntersectionY, pos_Cx, pos_Cy);
  return true;
}

bool CutAB_BC(float* positivePoints, int &positiveTriangleCount, int positiveTriangleCapacity, int positiveTriangleIndex, double ABIntersectionX, double ABIntersectionY, double BCIntersectionX, double BCIntersectionY)
{
  double pos_Ax = positivePoints[(positiveTriangleIndex * (3 * 2)) + 0];
  double pos_Ay = positivePoints[(positiveTriangleIndex * (3 * 2)) + 1];
  double pos_Bx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 2];
  double pos_By = positivePoints[(positiveTriangleIndex * (3 * 2)) + 3];
  double pos_Cx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 4];
  double pos_Cy = positivePoints[(positiveTriangleIndex * (3 * 2)) + 5];

  if (positiveTriangleCount + 2/*Replacing one triangle, adding two new*/ > positiveTriangleCapacity)
    return false;//Cannot add the three new triangles

  if (PointEqual(ABIntersectionX, ABIntersectionY, BCIntersectionX, BCIntersectionY))
  {
    return true;
  }

  if (PointEqual(ABIntersectionX, ABIntersectionY, pos_Ax, pos_Ay)
    || PointEqual(ABIntersectionX, ABIntersectionY, pos_Bx, pos_By)
    || PointEqual(ABIntersectionX, ABIntersectionY, pos_Cx, pos_Cy))
  {
    if (IsPointInTriangle(BCIntersectionX, BCIntersectionY, pos_Ax, pos_Ay, pos_Bx, pos_By, pos_Cx, pos_Cy))
      return true;//Mistaken intersection
  }

  if (PointEqual(BCIntersectionX, BCIntersectionY, pos_Ax, pos_Ay)
    || PointEqual(BCIntersectionX, BCIntersectionY, pos_Bx, pos_By)
    || PointEqual(BCIntersectionX, BCIntersectionY, pos_Cx, pos_Cy))
  {
    if (IsPointInTriangle(ABIntersectionX, ABIntersectionY, pos_Ax, pos_Ay, pos_Bx, pos_By, pos_Cx, pos_Cy))
      return true;//Mistaken intersection
  }

  if (LinesEqual(ABIntersectionX, ABIntersectionY, BCIntersectionX, BCIntersectionY, pos_Ax, pos_Ay, pos_Bx, pos_By))
  {
    //Both intersection points are on the AB line of the triangle, so no need to cut
    return true;
  }

  if (LinesEqual(ABIntersectionX, ABIntersectionY, BCIntersectionX, BCIntersectionY, pos_Ax, pos_Ay, pos_Cx, pos_Cy))
  {
    //Both intersection points are on the AB line of the triangle, so no need to cut
    return true;
  }

  if (LinesEqual(ABIntersectionX, ABIntersectionY, BCIntersectionX, BCIntersectionY, pos_Bx, pos_By, pos_Cx, pos_Cy))
  {
    //Both intersection points are on the AB line of the triangle, so no need to cut
    return true;
  }

  SetTriangle(positivePoints, positiveTriangleIndex, pos_Ax, pos_Ay, ABIntersectionX, ABIntersectionY, pos_Cx, pos_Cy);
  SetTriangle(positivePoints, positiveTriangleCount++, ABIntersectionX, ABIntersectionY, pos_Bx, pos_By, BCIntersectionX, BCIntersectionY);
  SetTriangle(positivePoints, positiveTriangleCount++, pos_Cx, pos_Cy, ABIntersectionX, ABIntersectionY, BCIntersectionX, BCIntersectionY);
  return true;
}

bool CutAC_BC(float* positivePoints, int &positiveTriangleCount, int positiveTriangleCapacity, int positiveTriangleIndex, double ACIntersectionX, double ACIntersectionY, double BCIntersectionX, double BCIntersectionY)
{
  double pos_Ax = positivePoints[(positiveTriangleIndex * (3 * 2)) + 0];
  double pos_Ay = positivePoints[(positiveTriangleIndex * (3 * 2)) + 1];
  double pos_Bx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 2];
  double pos_By = positivePoints[(positiveTriangleIndex * (3 * 2)) + 3];
  double pos_Cx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 4];
  double pos_Cy = positivePoints[(positiveTriangleIndex * (3 * 2)) + 5];

  if (positiveTriangleCount + 2/*Replacing one triangle, adding two new*/ > positiveTriangleCapacity)
    return false;//Cannot add the three new triangles

  if (PointEqual(ACIntersectionX, ACIntersectionY, BCIntersectionX, BCIntersectionY))
  {
    return true;
  }

  if (PointEqual(ACIntersectionX, ACIntersectionY, pos_Ax, pos_Ay)
    || PointEqual(ACIntersectionX, ACIntersectionY, pos_Bx, pos_By)
    || PointEqual(ACIntersectionX, ACIntersectionY, pos_Cx, pos_Cy))
  {
    if (IsPointInTriangle(BCIntersectionX, BCIntersectionY, pos_Ax, pos_Ay, pos_Bx, pos_By, pos_Cx, pos_Cy))
      return true;//Mistaken intersection
  }

  if ( PointEqual(BCIntersectionX, BCIntersectionY, pos_Ax, pos_Ay)
    || PointEqual(BCIntersectionX, BCIntersectionY, pos_Bx, pos_By)
    || PointEqual(BCIntersectionX, BCIntersectionY, pos_Cx, pos_Cy))
  {
    if (IsPointInTriangle(ACIntersectionX, ACIntersectionY, pos_Ax, pos_Ay, pos_Bx, pos_By, pos_Cx, pos_Cy))
      return true;//Mistaken intersection
  }

  if (LinesEqual(ACIntersectionX, ACIntersectionY, BCIntersectionX, BCIntersectionY, pos_Ax, pos_Ay, pos_Bx, pos_By))
  {
    //Both intersection points are on the AB line of the triangle, so no need to cut
    return true;
  }

  if (LinesEqual(ACIntersectionX, ACIntersectionY, BCIntersectionX, BCIntersectionY, pos_Ax, pos_Ay, pos_Cx, pos_Cy))
  {
    //Both intersection points are on the AB line of the triangle, so no need to cut
    return true;
  }

  if (LinesEqual(ACIntersectionX, ACIntersectionY, BCIntersectionX, BCIntersectionY, pos_Bx, pos_By, pos_Cx, pos_Cy))
  {
    //Both intersection points are on the AB line of the triangle, so no need to cut
    return true;
  }

  SetTriangle(positivePoints, positiveTriangleIndex, pos_Ax, pos_Ay, pos_Bx, pos_By, ACIntersectionX, ACIntersectionY);
  SetTriangle(positivePoints, positiveTriangleCount++, ACIntersectionX, ACIntersectionY, pos_Bx, pos_By, BCIntersectionX, BCIntersectionY);
  SetTriangle(positivePoints, positiveTriangleCount++, BCIntersectionX, BCIntersectionY, pos_Cx, pos_Cy, ACIntersectionX, ACIntersectionY);
  return true;
}

bool LineIntersect(float* positivePoints, int &positiveTriangleCount, int positiveTriangleCapacity, int positiveTriangleIndex, float Ax, float Ay, float Bx, float By)
{
  double pos_Ax = positivePoints[(positiveTriangleIndex * (3 * 2)) + 0];
  double pos_Ay = positivePoints[(positiveTriangleIndex * (3 * 2)) + 1];
  double pos_Bx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 2];
  double pos_By = positivePoints[(positiveTriangleIndex * (3 * 2)) + 3];
  double pos_Cx = positivePoints[(positiveTriangleIndex * (3 * 2)) + 4];
  double pos_Cy = positivePoints[(positiveTriangleIndex * (3 * 2)) + 5];

  //Check if at least one point is inside the triangle, if so we can (and must, see below) perform ray-segment intersection rather than segment-segment intersection
  //Wht MUST we use ray-segment intersection? - What if both points are inside the triangle? Since both points A and B are inside the triangle, there
  //                                            is no intersection between the A-B line and any lines on the triangle. But since the line is inside the triangle,
  //                                            we must still cut the triangle.
  bool definitelyIntersects = IsPointInTriangle(Ax, Ay, pos_Ax, pos_Ay, pos_Bx, pos_By, pos_Cx, pos_Cy) ||
    IsPointInTriangle(Bx, By, pos_Ax, pos_Ay, pos_Bx, pos_By, pos_Cx, pos_Cy);

  double ABIntersectionX, ABIntersectionY;
  if (CheckIntersection(Ax, Ay, Bx, By, definitelyIntersects, pos_Ax, pos_Ay, pos_Bx, pos_By, ABIntersectionX, ABIntersectionY))
  {
    double ACIntersectionX, ACIntersectionY;
    if (CheckIntersection(Ax, Ay, Bx, By, definitelyIntersects, pos_Ax, pos_Ay, pos_Cx, pos_Cy, ACIntersectionX, ACIntersectionY))
    {
      //Line cuts across AB and AC @ACIntersection and @ABIntersection
      return CutAB_AC(positivePoints, positiveTriangleCount, positiveTriangleCapacity, positiveTriangleIndex, ABIntersectionX, ABIntersectionY, ACIntersectionX, ACIntersectionY);
    }
    double BCIntersectionX, BCIntersectionY;
    if (CheckIntersection(Ax, Ay, Bx, By, definitelyIntersects, pos_Bx, pos_By, pos_Cx, pos_Cy, BCIntersectionX, BCIntersectionY))
    {
      //Line cuts across AB and BC @BCIntersection and @ABIntersection
      return CutAB_AC(positivePoints, positiveTriangleCount, positiveTriangleCapacity, positiveTriangleIndex, ABIntersectionX, ABIntersectionY, BCIntersectionX, BCIntersectionY);
    }
  }
  
  double ACIntersectionX, ACIntersectionY;
  if (CheckIntersection(Ax, Ay, Bx, By, definitelyIntersects, pos_Ax, pos_Ay, pos_Cx, pos_Cy, ACIntersectionX, ACIntersectionY))
  {
    //Do not need to check AB intersection, that was already checked above.
    //So the only choice left is BC intersection
    double BCIntersectionX, BCIntersectionY;
    if (CheckIntersection(Ax, Ay, Bx, By, definitelyIntersects, pos_Bx, pos_By, pos_Cx, pos_Cy, BCIntersectionX, BCIntersectionY))
    {
      //Line cuts across AC and BC @BCIntersection and @ACIntersection
      return CutAC_BC(positivePoints, positiveTriangleCount, positiveTriangleCapacity, positiveTriangleIndex, ACIntersectionX, ACIntersectionY, BCIntersectionX, BCIntersectionY);
    }
  }
 
  return true;//No cuts were made, but return code indicates success, and no cut is still success
}

int main()
{
  return 0;
}