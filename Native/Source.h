#pragma once

#include <cmath>
#include <Windows.h>//TODO: Remove this! Used for Beep()
extern "C"
{
  __declspec(dllexport) bool CutTriangles(int& startNegativeTriangleIndex, float* positivePoints, int &positiveTriangleCount, int positiveTriangleCapacity, float* negativePoints, int negativeTriangleCount, int negativeTriangleCapacity);
}

bool LineIntersect(float* positivePoints, int &positiveTriangleCount, int positiveTriangleCapacity, int positiveTriangleIndex, float Ax, float Ay, float Bx, float By);